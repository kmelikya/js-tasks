import { Injectable } from '@nestjs/common';
import { Pool } from 'pg';
import { UsersTable, UsersTableItem } from './tableInfo'

@Injectable()
export class AppService {
  
  pool: Pool = new Pool({
    user: 'postgres',
    host: 'localhost',
    database: 'nesttaskdb',
    password: 'test',
    port: 5432,
  });

  tableInfo: UsersTable = new UsersTable();

  async getAllUsers(limit: number = 10, offset: number = 0) {
    let query: string = `SELECT * FROM ${this.tableInfo.tableName} LIMIT ${limit} OFFSET ${offset}`;
    const res = await this.pool.query(query);
    return res.rows;
  }

  async getUserById(id: number) {
    // TODO: fix 0 part
    const res = await this.pool.query(`SELECT * FROM ${this.tableInfo.tableName} WHERE ${this.tableInfo.columns[0]} = ${id};`);
    return res.rows;
  }

  async addNewUser(item: UsersTableItem) {
    if (!item.allFieldPassed()) {
      throw new Error('For adding new user all information should be provided');
    } else if (!item.validateEmail()) {
      throw new Error('Wrong email format');
    }

    let query: string = `INSERT INTO ${this.tableInfo.tableName} VALUES (${Array.from(item._columnsToValuesMap.values()).map(element => `'${element}'`).join(', ')});`;
    await this.pool.query(query);
  }

  async updateUser(id:number, item: UsersTableItem) {
    // TODO: fix 0 part
    // TODO: id in body?
    // TODO: should all checks be here or in controller?
    if (!item.allFieldPassed()) {
      throw new Error('For adding new user all information should be provided');
    }

    // partial update doesn't check all fields passing which is required for this request
    await this.updateUserPartial(id, item);
  }

  async updateUserPartial(id:number, item: UsersTableItem) {
    // TODO: fix 0 part
    if (!item.validateEmail()) {
      throw new Error('Wrong email format');
    } else if ((await this.getUserById(id)).length === 0) {
      throw new Error('User with given id doesn\'t exist');
    }

    let arr: Array<string> = new Array();
    item._columnsToValuesMap.forEach((value, key) => arr.push(`${key} = '${value}'`));
    let query: string = `UPDATE ${this.tableInfo.tableName} SET ${arr.join(', ')} WHERE ${this.tableInfo.columns[0]} = ${id};`;
    await this.pool.query(query);
  }

  async deleteUser(id:number) {
    // TODO: fix 0 part
    if ((await this.getUserById(id)).length === 0) {
      throw new Error('User with given id doesn\'t exist');
    }
    await this.pool.query(`DELETE FROM ${this.tableInfo.tableName} WHERE ${this.tableInfo.columns[0]} = ${id};`);
  }

}


/*
{
    "id" : 0, 
    "first_name" : "aaa",
    "last_name" : "bbb",
    "email" : "ccc@gmail.com",
    "password": "xxx",
    "age": 7
}
*/
