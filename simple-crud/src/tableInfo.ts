import { Injectable } from '@nestjs/common';

abstract class TableInfo {
	tableName: string;
	columns: Array<string>;
}

abstract class TableItem {
	_tableInfo: TableInfo;
	_columnsToValuesMap: Map<string, string>;
}

@Injectable()
class UsersTable extends TableInfo {
	tableName = 'users';
	columns = ['id', 'first_name', 'last_name', 'email', 'password', 'age'];
}

class UsersTableItem extends TableItem {
	_tableInfo = new UsersTable();
	_columnsToValuesMap = new Map();

	constructor(body: any) {
		super();
		for (let [key, value] of Object.entries(body)) {
			if (!this._tableInfo.columns.includes(key)) {
				throw Error("Unexisting column name");
			}
			this._columnsToValuesMap.set(key, value);
		}
	}

	validateEmail(): boolean {
		// TODO: fix 3
		if (!this._columnsToValuesMap.has(this._tableInfo.columns[3]))
			return true;

		let email: string = this._columnsToValuesMap.get(this._tableInfo.columns[3]);
		const re = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
	}

	allFieldPassed(): boolean {
		if (this._columnsToValuesMap.size !== this._tableInfo.columns.length)
			return false;

		for (let columnName of this._tableInfo.columns) {
			if (!this._columnsToValuesMap.has(columnName)) {
				return false;
			}
		}

		return true;
	}
}

export { UsersTable, UsersTableItem };