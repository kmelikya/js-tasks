import { Controller, Get, Post, Put, Patch, Delete, Query, Param, Body, HttpException, HttpStatus } from '@nestjs/common';
import { AppService } from './app.service';
import { UsersTableItem } from './tableInfo'

interface AllUsersQueryArgs {
  limit?: number;
  offset?: number;
}

interface PostRequestBody {
  
}

interface PatchRequestBody {
  
}

@Controller('users')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  async getAllUsers(@Query() args: AllUsersQueryArgs) {
    const {limit, offset} = args;
    return await this.appService.getAllUsers(limit, offset);
  }

  @Get('/:id')
  async getUserById(@Param('id') id: number) {
    let userInfo = await this.appService.getUserById(id);
    if (userInfo.length === 0) {
      throw new HttpException('User with given id doesn\'t exist', HttpStatus.BAD_REQUEST);
    }
    return userInfo;
  }

  @Post()
  // TODO: not any type, PostRequestBody as needed
  async createUser(@Body() body: any) {
    try {
      await this.appService.addNewUser(new UsersTableItem(body));
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
    return 'User is successfully created';
  }

  @Put('/:id')
  // TODO: not any type, PostRequestBody as needed
  async updateUserInfoById(@Param('id') id: number, @Body() body: any) {
    try {
      await this.appService.updateUser(id, new UsersTableItem(body));
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
    return 'User info is successfully updated';
  }

  @Patch('/:id')
  // TODO: not any type, PatchRequestBody as needed
  async updateUserInfoPartialById(@Param('id') id: number, @Body() body: any) {
    try {
      await this.appService.updateUserPartial(id, new UsersTableItem(body));
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
    return 'User info is successfully updated';
  }

  @Delete('/:id')
  async deleteUserById(@Param('id') id: number) {
    try { 
      await this.appService.deleteUser(id);
    } catch (error) {
      throw new HttpException(error.message, HttpStatus.BAD_REQUEST);
    }
    return 'User is successfully deleted';
  }
}