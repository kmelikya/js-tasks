type ResolveCallback = (x : any) => any;
type RejectCallback = (x : any) => any;
type PromiseCallback = (resolveCallback: ResolveCallback, rejectCallback: RejectCallback) => any;

enum State {
    Pending = 'pending',
    Fulfilled = 'fulfilled',
    Rejected = 'rejected',
}

export class CustomPromise {

    state: State;
    resolveCallback: any;
    rejectCallback: any;
    defaultCallback: any;
    nextPromise?: CustomPromise;
    isCallbackSet: boolean;

    constructor(callback?: PromiseCallback) {

        this.state = State.Pending;
        this.isCallbackSet = false;

        let reject = (reason: any) => {
            if (this.state !== State.Pending)
                return;
            
            try {
                let retValue = this.rejectCallback(reason);
                this.state = State.Fulfilled;
                if (this.nextPromise?.isCallbackSet)
                    this.nextPromise.defaultCallback(retValue, this.state);
            } catch (err) {
                this.state = State.Rejected;
                if (this.nextPromise?.isCallbackSet)
                    this.nextPromise.defaultCallback(reason, this.state);
            }
        };

        let resolve = (value : any) => {
            if (this.state !== State.Pending)
                return;

            try {
                let retValue = this.resolveCallback(value);
                this.state = State.Fulfilled;
                if (this.nextPromise?.isCallbackSet)
                    this.nextPromise.defaultCallback(retValue, this.state);
            } catch (err) {
                this.state = State.Rejected;
                if (this.nextPromise?.isCallbackSet)
                    this.nextPromise.defaultCallback(err, this.state);
            }
        };

        this.defaultCallback = (value: any, state: string) => {
            if (state === State.Fulfilled) {
                resolve(value);
            } else {
                reject(value);
            }
        };

        if (callback)
            callback(resolve, reject);
    }

    then(resolveCallback: ResolveCallback, rejectCallback?: RejectCallback) {
        this.isCallbackSet = true;
        this.resolveCallback = resolveCallback;
        if (rejectCallback)
            this.rejectCallback = rejectCallback;
        this.nextPromise = new CustomPromise();
        return this.nextPromise;
    }

    catch(callback: RejectCallback) {
        this.isCallbackSet = true;
        this.rejectCallback = callback;
        this.nextPromise = new CustomPromise();
        return this.nextPromise;
    }

}