import { CustomPromise } from './CustomPromise' 

function sleepAndResolve(delay: number, functionName: string) {
    return new CustomPromise((resolve, reject) => {
        setTimeout(() => { console.log(functionName + " started"); resolve(delay) }, delay);
    });
}

function sleepAndReject(delay: number, functionName: string) {
    return new CustomPromise((resolve, reject) => {
        setTimeout(() => { console.log(functionName + " started"); reject(delay) }, delay);
    });
}

function singleThen() {
    sleepAndResolve(3000, "singleThen")
    .then((x) => {
        console.log("then");
    });
}

function singleCatch() {
    sleepAndReject(3000, "singleCatch")
    .catch((x) => {
        console.log("catch");
    });
}

function multipleThen() {
    sleepAndResolve(3000, "multipleThen")
    .then((delayMilliseconds) => {
        console.log("then1");
        return delayMilliseconds / 1000;
    })
    .then((seconds) => {
        console.log("then2");
        if (!isFinite(seconds)) {
            throw new Error('Invalid seconds are passed');
        }

        return seconds / 60;
    })
    .then((minutes) => {
        console.log("then3");
        if (!isFinite(minutes)) {
            throw new Error('Invalid minutes are passed');
        }

        return minutes / 60;
    })
    .then((hours) => {
        console.log("then4");
        if (!isFinite(hours)) {
            throw new Error('Invalid hours are passed');
        }

        console.log(hours);
    });
}

function thenAndCatchByResolve() {
    sleepAndResolve(3000, "thenAndCatchByResolve")
    .then((x) => {
        console.log("then");
    })
    .catch((x) => {
        console.log("catch");
    });
}

function thenAndCatchByReject() {
    sleepAndReject(3000, "thenAndCatchByReject")
    .then((x) => {
        console.log("then");
    })
    .catch((x) => {
        console.log("catch");
    });
}

function multipleThenAndCatch() {
    sleepAndResolve(3000, "multipleThenAndCatch")
    .then((delayMilliseconds) => {
        console.log("then1");
        return delayMilliseconds / 1000;
    })
    .then((seconds) => {
        console.log("then2");
        if (!isFinite(seconds)) {
            throw new Error('Invalid seconds are passed');
        }

        return seconds / 60;
    })
    .then((minutes) => {
        console.log("then3");
        if (!isFinite(minutes)) {
            throw new Error('Invalid minutes are passed');
        }

        return minutes / 60;
    })
    .then((hours) => {
        console.log("then4");
        if (!isFinite(hours)) {
            throw new Error('Invalid hours are passed');
        }

        console.log(hours);
    })
    .catch((err) => {
        console.log("catch")
    });
}

function throwInThen() {
    sleepAndResolve(3000, "throwInThen")
    .then((x) => {
        console.log("then");
        throw new Error("should call catch clause");
    })
    .catch((x) => {
        console.log("catch");
    });
}

function throwInFirstThenAndHaveThenAfterThat() {
    sleepAndResolve(3000, "throwInFirstThenAndHaveThenAfterThat")
    .then((x) => {
        console.log("then1");
        throw new Error("should call catch clause");
    })
    .then((delayMilliseconds) => {
        console.log("then2");
        return delayMilliseconds / 1000;
    })
    .catch((x) => {
        console.log("catch");
    });
}

function throwInThenAndHaveThenAfterThat() {
    sleepAndResolve(3000, "throwInThenAndHaveThenAfterThat")
    .then((delayMilliseconds) => {
        console.log("then1");
        return delayMilliseconds / 1000;
    })
    .then((delayMilliseconds) => {
        console.log("then2");
        return delayMilliseconds / 1000;
    })
    .then((x) => {
        console.log("then3");
        throw new Error("should call catch clause");
    })
    .then((delayMilliseconds) => {
        console.log("then4");
        return delayMilliseconds / 1000;
    })
    .then((delayMilliseconds) => {
        console.log("then5");
        return delayMilliseconds / 1000;
    })
    .catch((x) => {
        console.log("catch");
        console.log(x);
    });
}

function catchAndThenAfterThat() {
    sleepAndReject(3000, "throwInThenAndHaveThenAfterThat")
    .catch((x) => {
        console.log("catch1");
    })
    .then((delayMilliseconds) => {
        console.log("then1");
        return delayMilliseconds / 1000;
    })
    .then((delayMilliseconds) => {
        console.log("then2");
        return delayMilliseconds / 1000;
    })
    .then((delayMilliseconds) => {
        console.log("then3");
        throw new Error("should call catch clause");
    })
    .then((delayMilliseconds) => {
        console.log("then4");
        return delayMilliseconds / 1000;
    })
    .then((delayMilliseconds) => {
        console.log("then5");
        return delayMilliseconds / 1000;
    })
    .catch((x) => {
        console.log("catch2");
    })
    .then((delayMilliseconds) => {
        console.log("then6");
        return delayMilliseconds / 1000;
    })
    .catch((x) => {
        console.log("catch3");
    });
}

// singleThen();
// singleCatch();
// multipleThen();
// thenAndCatchByResolve();
// thenAndCatchByReject();
// multipleThenAndCatch();
// throwInThen();
// throwInFirstThenAndHaveThenAfterThat();
// throwInThenAndHaveThenAfterThat();
catchAndThenAfterThat();

// let pr = new Promise();

// pr.then();
// pr.then();
// pr.then();

/*
pr
.then()
.then()
.then()
.then()
.then();

pr
.then()
.then()
.then()
.then()
.then();

pr
.then()
.then()
.then()
.then()
.then();
*/