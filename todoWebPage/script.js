(function () {
    let taskCount = 0;
    let doneCount = 0;
    
    function getItemCheckbox(todoItem) {
        return todoItem.getElementsByClassName("checkbox")[0];
    }
    
    function getItemText(todoItem) {
        return todoItem.getElementsByClassName("text")[0];
    }
    
    function getItemButton(todoItem) {
        return todoItem.getElementsByClassName("button")[0];
    }
    
    function checkStateHanlder(checkboxItem, textItem) {
        if (checkboxItem.checked) {
            ++doneCount;
            textItem.style["text-decoration-line"] = "line-through";
            textItem.style["opacity"] = "0.5";
        } else {
            --doneCount;
            textItem.style.removeProperty("text-decoration-line");
            textItem.style["opacity"] = "1";
        }
        updateListFooter();
    }
    
    function addCheckboxEventListener(todoItem) {
        let checkboxItem = getItemCheckbox(todoItem);
        let textItem = getItemText(todoItem);
    
        checkboxItem.addEventListener("click", function(event) {
            checkStateHanlder(checkboxItem, textItem);
        });
    }
    
    function addCloseButtonListener(todoItem) {
        let buttonItem = getItemButton(todoItem);
        buttonItem.addEventListener("click", function(event) {
            --taskCount;
            if (getItemCheckbox(todoItem).checked)
                --doneCount;
            todoItem.remove();
            updateListFooter();
        });
    }
    
    function createNewTodoItem(title) {
        // todo-item
        const todoItem = document.createElement("div");
        todoItem.setAttribute("class", "todo-item");
    
        // checkbox
        const checkboxItem = document.createElement("input");
        checkboxItem.setAttribute("type", "checkbox");
        checkboxItem.setAttribute("class", "checkbox");
    
        // text
        const textItem = document.createElement("div");
        textItem.setAttribute("class", "text");
        textItem.innerText = title;
    
        // button
        const buttonItem = document.createElement("button");
        buttonItem.setAttribute("class", "button");
        buttonItem.innerText = "x";
    
        // add children
        todoItem.appendChild(checkboxItem);
        todoItem.appendChild(textItem);
        todoItem.appendChild(buttonItem);
        addCheckboxEventListener(todoItem);
        addCloseButtonListener(todoItem);
    
        // add item to HTML
        document.getElementById("list").appendChild(todoItem);
        ++taskCount;
        updateListFooter();
    }
    
    function addInputFieldEventListener() {
        let input = document.getElementById("task-name");
        input.addEventListener("keyup", function(event) {
            if (event.key === 'Enter') {
                event.preventDefault();
                createNewTodoItem(this.value)
                this.value = "";
            }
        });
    }
    
    function toggleAllCheckboxes(check) {
        let todoItems = document.getElementById("list").getElementsByClassName("todo-item");
        for (let item of todoItems) {
            let checkboxItem = getItemCheckbox(item);
            let textItem = getItemText(item);
    
            if (checkboxItem.checked === check)
                continue;
    
            checkboxItem.checked = check;
            checkStateHanlder(checkboxItem, textItem);
        }
    }
    
    function addCheckAllButtonListener() {
        let button = document.getElementById("check-all-button");
        button.addEventListener("click", function(event) {
            if (doneCount === taskCount) {
                // uncheck all
                toggleAllCheckboxes(false);
            } else {
                // check all
                toggleAllCheckboxes(true);
            }
        });
    }
    
    function updateListFooter() {
        let listFooter = document.getElementById("list-footer");
        listFooter.innerText = String((taskCount - doneCount) + " tasks left");
    }

    function addAllButtonListener() {
        let allButton = document.getElementById("all-button");
        allButton.addEventListener("click", function(event) {
            let todoItems = document.getElementById("list").getElementsByClassName("todo-item");
            for (let item of todoItems) {
                item.style.display = 'flex';
            }
        });
    }

    function addActiveButtonListener() {
        let activeButton = document.getElementById("active-button");
        activeButton.addEventListener("click", function(event) {
            let todoItems = document.getElementById("list").getElementsByClassName("todo-item");
            for (let item of todoItems) {
                if (getItemCheckbox(item).checked)
                    item.style.display = 'none';
                else
                    item.style.display = 'flex';
            }
        });
    }

    function addCompletedButtonListener() {
        let completedButton = document.getElementById("completed-button");
        completedButton.addEventListener("click", function(event) {
            let todoItems = document.getElementById("list").getElementsByClassName("todo-item");
            for (let item of todoItems) {
                if (!getItemCheckbox(item).checked)
                    item.style.display = 'none';
                else
                    item.style.display = 'flex';
            }
        });
    }

    function addFooterButtonsListeners() {
        addAllButtonListener();
        addActiveButtonListener();
        addCompletedButtonListener();
    }
    
    addInputFieldEventListener();
    addCheckAllButtonListener();
    addFooterButtonsListeners();
    updateListFooter();
})();
