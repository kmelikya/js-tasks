import * as http from 'http';

interface IncomingMessage extends http.IncomingMessage {
    body?: any;
}

type CallbackFunction = (req: IncomingMessage, res: http.ServerResponse) => any;

class CallbackInfo {
    _path: string;
    _callback: CallbackFunction;
    
    constructor(path: string, callback: CallbackFunction) {
        this._path = path;
        this._callback = callback;
    }
}

export class HttpApp {

    static _getMethodName: string = 'GET';
    static _postMethodName: string = 'POST';
    static _putMethodName: string = 'PUT';
    static _patchMethodName: string = 'PATCH';
    static _deleteMethodName: string = 'DELETE';
    _methodToCallbacksMap: Map<string, Set<CallbackInfo>> = new Map();

    setCallbackForMethod(methodName: string, path: string, callback: CallbackFunction) {
        if (!this._methodToCallbacksMap.has(methodName)) {
            this._methodToCallbacksMap.set(methodName, new Set());
        }
        
        this._methodToCallbacksMap.get(methodName)?.add(new CallbackInfo(path, callback));
    }

    get(path: string, callback: CallbackFunction) {
        this.setCallbackForMethod(HttpApp._getMethodName, path, callback);
    }

    post(path: string, callback: CallbackFunction) {
        this.setCallbackForMethod(HttpApp._postMethodName, path, callback);
    }

    put(path: string, callback: CallbackFunction) {
        this.setCallbackForMethod(HttpApp._putMethodName, path, callback);
    }

    patch(path: string, callback: CallbackFunction) {
        this.setCallbackForMethod(HttpApp._patchMethodName, path, callback);
    }

    delete(path: string, callback: CallbackFunction) {
        this.setCallbackForMethod(HttpApp._deleteMethodName, path, callback);
    }

    async constructRequestBody(req: IncomingMessage) {
        let data = '';
        let body: any;

        await new Promise<void>((resolve) => {
            req.on('data', chunk => {
                data += chunk;
            });
    
            req.on('end', () => {
                body = JSON.parse(data);
                resolve();
            });
        });

        req.body = body;
    }

    listen(port: number) {
        http.createServer(async (req: IncomingMessage, res) => {
            if (!req.method || !req.url)
                return;

            if (req.method !== 'GET') {
                await this.constructRequestBody(req);
            }

            let methodCallbacks = this._methodToCallbacksMap.get(req.method);
            if (methodCallbacks) {
                for (let callbackInfo of methodCallbacks) {
                    if (callbackInfo._path === req.url) {
                        callbackInfo._callback(req, res);
                        return;
                    }
                }
            }

            res.writeHead(404);
            res.end();
        }).listen(port);
    }

}