import { HttpApp } from './HttpApp';

const httpApp = new HttpApp();

httpApp.get('/abc', function (request, response) {
    console.log('get abc');
    response.end();
  // My code
});

httpApp.get('/def', function (request, response) {
  // My code
  console.log('get def');
  response.end();
});

httpApp.post('/abc', function (request, response) {
//   request.body // I should have body of response here attached, in non get requests
    console.log('post abc');
    console.log(request.body);
    response.end();
});

httpApp.listen(3000);