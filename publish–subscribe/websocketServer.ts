import WebSocket, { Server as WebSocketServer } from 'ws';

enum RequestType {
	Subscribe = 'subscribe',
	Unsubscribe = 'unsubscribe',
	SendMessage = 'sendmessage'
}

interface UserRequest {
	username: string;
	channel: string;
	message?: string;
}

interface ActionRequest extends UserRequest {
	action: string;
}

class PublishSubscribeServer {

    _wss: WebSocketServer = new WebSocketServer({
			port: 8080
		});
		_channelToListenersMap: Map<string, Array<WebSocket>> = new Map();

		constructor() {
			this._wss.on('connection', (connection) => {
				console.log('New user connected');

				connection.on('message', (message) => {
					let actionRequest: ActionRequest = JSON.parse(message.toString());
					this.parseRequest(actionRequest, connection);
				});
			});
		}

		subscribeToChannel(connection: WebSocket, userRequest: UserRequest) {
			if (!this._channelToListenersMap.has(userRequest.channel)) {
				this._channelToListenersMap.set(userRequest.channel, new Array());
			}

			console.log(`User ${userRequest.username} subscribed to the channel ${userRequest.channel}`);
			this._channelToListenersMap.get(userRequest.channel)?.push(connection);
		}

		unsubscribeFromChannel(connection: WebSocket, userRequest: UserRequest) {
			if (!this._channelToListenersMap.has(userRequest.channel)) {
				throw new Error(`Channel ${userRequest.channel} does not exist`);
			}

			let index: number | undefined = this._channelToListenersMap.get(userRequest.channel)?.indexOf(connection);
			if (!index || index === -1) {
				throw new Error(`User is not subscribed to the channel ${userRequest.channel}`);
			}
			console.log(`User ${userRequest.username} unsubscribed from the channel ${userRequest.channel}`);
			this._channelToListenersMap.get(userRequest.channel)?.splice(index, 1);
		}

		sendMessageToChannel(userRequest: UserRequest) {
			if (!this._channelToListenersMap.has(userRequest.channel)) {
				throw new Error(`Channel ${userRequest.channel} does not exist`);
			}

			console.log(`User ${userRequest.username} sends message '${userRequest.message}' to the channel ${userRequest.channel}`);

			let listeners: Array<WebSocket> | undefined = this._channelToListenersMap.get(userRequest.channel);
			if (listeners) {
				for (let listener of listeners) {
					listener.send(`#${userRequest.channel} ${userRequest.username} : ${userRequest.message}`);
				}
			}

		}

		parseRequest(request: ActionRequest, connection: WebSocket) {
			switch (request.action) {
				case RequestType.Subscribe:
					this.subscribeToChannel(connection, request);
					break;
				case RequestType.Unsubscribe:
					this.unsubscribeFromChannel(connection, request);
					break;
				case RequestType.SendMessage:
					if (!request.message) {
						throw new Error("Message to send is missing");
					}

					this.sendMessageToChannel(request);
					break;
			}
		}

}


(function(){
	new PublishSubscribeServer();
})();
