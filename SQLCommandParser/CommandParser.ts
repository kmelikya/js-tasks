abstract class CommandParser {
    _query: string;

    constructor(query: string) {
        this._query = query;
    }

    abstract parseQuery(): void;
}

export { CommandParser };