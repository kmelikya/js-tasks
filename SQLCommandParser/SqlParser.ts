import { ParserFactory } from './ParserFactory';

(function(){
	let parser = ParserFactory.getParser(process.argv[2]);
    parser.parseQuery();
})();