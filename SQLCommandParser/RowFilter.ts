import { DBItem } from './DBItem';
import { Constants } from './Constants';

export class RowFilter {
    _columnName: string;
    _operation: string;
    _value: string;

    static equal : string = '=';
    static notEqual : string = '!=';
    static great : string = '>';
    static less : string = '<';
    static greatEqual : string = '>=';
    static lessEqual : string = '<=';

    constructor(columnName: string, operation: string, value: string) {
        this._columnName = columnName;
        this._operation = operation;
        this._value = value;
    }

    pass(dbItem: DBItem): boolean {
        let itemValue: string;
        switch (this._columnName) {
            case Constants.firstNameColumnName:
                itemValue = dbItem.getFirstName();
                break;
            case Constants.lastNameColumnName:
                itemValue = dbItem.getLastName();
                break;
            case Constants.ageColumnName:
                itemValue = dbItem.getAge();
                break;
            default:
                throw new Error("Unexisting column name");
        }

        switch (this._operation) {
            case RowFilter.equal:
                return itemValue == this._value;
            case RowFilter.notEqual:
                return itemValue != this._value;
            case RowFilter.great:
                return itemValue > this._value;
            case RowFilter.greatEqual:
                return itemValue >= this._value;
            case RowFilter.less:
                return itemValue < this._value;
            case RowFilter.lessEqual:
                return itemValue <= this._value;
            default:
                throw new Error("Unsupported operation");
        }
    }
}