import { Constants, ColumnIndex } from './Constants';

export class ColumnFilter {
    _columnNamesToShow: Array<string>;

    constructor(columnNames: Array<string>) {
        this._columnNamesToShow = columnNames;
    }

    filter(data: Array<Array<any>>) {
        let indicesToRemove: Array<number> = new Array();
        // push in reverse order to not have problems with deleting
        if (!this._columnNamesToShow.includes(Constants.ageColumnName)) {
            indicesToRemove.push(ColumnIndex.ageColumnIndex);
        }
        if (!this._columnNamesToShow.includes(Constants.lastNameColumnName)) {
            indicesToRemove.push(ColumnIndex.lastNameColumnIndex);
        }
        if (!this._columnNamesToShow.includes(Constants.firstNameColumnName)) {
            indicesToRemove.push(ColumnIndex.firstNameColumnIndex);
        }

        data.forEach((arr) => {
            for (let index of indicesToRemove) {
                arr.splice(index, 1);
            }
        });
    }
}