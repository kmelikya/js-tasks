export class DBItem {
    private _firstName : string = 'null';
    private _lastName : string = 'null';
    private _age : string = 'null';

    constructor(firstName ?: string, lastName ?: string, age ?: string | number) {
        if (firstName) {
            this.setFirstName(firstName);
        }
        if (lastName) {
			this.setLastName(lastName);
        }
        if (age) {
			this.setAge(age);
        }
    }

    setFirstName(firstName : string) {
		this._firstName = firstName;
    }

    setLastName(lastName : string) {
		this._lastName = lastName;
    }

    setAge(age : string | number) {
        if (typeof(age) === 'number') {
            this._age = String(age);
        } else if (typeof(age) === 'string') {
            this._age = age;
        }
    }

    getFirstName(): string {
        return this._firstName;
    }

    getLastName(): string {
        return this._lastName;
    }

    getAge(): string {
        return this._age;
    }

    getDataAsCSV() {
        return `${this.getFirstName()}, ${this.getLastName()}, ${this.getAge()}`
    }
}