import fs from 'fs/promises';
import { DBItem } from './DBItem';
import { RowFilter } from './RowFilter';
import { ColumnFilter } from './ColumnFilter';
import { Constants } from './Constants';

class DB {

    createDBItem(columns : Array<string>, values : Array<string>): DBItem {
        if (columns.length !== values.length) {
            throw new Error("Invalid count of insert fields or values");
        }
    
        let dbItem = new DBItem();
    
        for (let i = 0; i < columns.length; ++i) {
            let value : string = values[i];
            switch (columns[i]) {
                case Constants.firstNameColumnName:
                    dbItem.setFirstName(value);
                    break;
                case Constants.lastNameColumnName:
                    dbItem.setLastName(value);
                    break;
                case Constants.ageColumnName:
                    dbItem.setAge(value);
                    break;
            }
        }
    
        return dbItem;
    }

    async saveDBItem(dbItem : DBItem) {
        await fs.appendFile(Constants.fileName, dbItem.getDataAsCSV() + '\n');
    }

    private async getAllData() {
        let data = await fs.readFile(Constants.fileName);
        let array = data.toString().split("\n");
        if (array[array.length - 1] === '') {
            array.pop();
        }
        let dataAsMultidimensionalArray = array.map((element) => element.split(', '));
        return dataAsMultidimensionalArray;
    }

    async getData(rowFilter?: RowFilter, columnFilter?: ColumnFilter) {
        let data = await this.getAllData();
        if (!rowFilter && !columnFilter)
            return data;

        if (columnFilter) {
            columnFilter.filter(data);
        }

        if (rowFilter) {
            let filteredData: string[][] = [];

            for (let row of data) {
                if (rowFilter.pass(new DBItem(...row))) {
                    filteredData.push(row);
                }
            }

            return filteredData;
        }

        return data;
    }
}

export { DB };