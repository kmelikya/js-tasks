enum Constants {
    firstNameColumnName = 'first_name',
    lastNameColumnName = 'last_name',
    ageColumnName = 'age',
    fileName = 'tmp.txt',

    insertKeyword = 'INSERT',
    selectKeyword = 'SELECT',
    valuesKeyword = 'VALUES',
    whereKeyword = 'WHERE',
}

enum ColumnIndex {
    firstNameColumnIndex,
    lastNameColumnIndex,
    ageColumnIndex,
}

export { Constants, ColumnIndex };