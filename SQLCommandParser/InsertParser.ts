import { CommandParser } from './CommandParser';
import { DBItem } from './DBItem';
import { DB } from './DB';
import { Constants } from './Constants';

export class InsertParser extends CommandParser {

    getInsertFieldValues(fieldValues : string): Array<string> {
        if (fieldValues.indexOf('(') === -1) {
            if (fieldValues.indexOf(',') !== -1) {
                throw new Error("Multiple column names should be written using parenthesis");
            }
            return [fieldValues];
        }
    
        if (fieldValues.indexOf(')') === -1) {
            throw new Error("Enclosing bracket is missing");
        }
    
        if (fieldValues.indexOf('(') !== 0 || fieldValues.indexOf(')') !== fieldValues.length - 1) {
            throw new Error("Incorrect brackets");
        }
    
        return fieldValues.substring(1, fieldValues.length - 1).split(', ');
    }
    
    getInsertValues(insertValues : string): Array<string> {
        return this.getInsertFieldValues(insertValues);
    }
    
    getColumnNames(insertFields : string): Array<string> {
        return this.getInsertFieldValues(insertFields);
    }

    async parseQuery() {
        let dbInstance = new DB();

        let insertFields : string = this._query.substring(this._query.indexOf(Constants.insertKeyword) + Constants.insertKeyword.length, this._query.indexOf(Constants.valuesKeyword)).trim();
        let columns : Array<string> = this.getColumnNames(insertFields);
        console.log(columns);
    
        let closeBracketIndex = this._query.indexOf(')', this._query.indexOf(Constants.valuesKeyword));
        let semicolonIndex = (this._query.indexOf(';', this._query.indexOf(Constants.valuesKeyword)));
        let lastIndex : number = (closeBracketIndex === -1 ? (semicolonIndex === -1 ? (this._query.length - 1) : semicolonIndex) : closeBracketIndex) + 1;
        let insertValues : string = this._query.substring(this._query.indexOf(Constants.valuesKeyword) + Constants.valuesKeyword.length, lastIndex).trim();
        let values : Array<string> = this.getColumnNames(insertValues);
        console.log(values);
    
        let dbItem: DBItem = dbInstance.createDBItem(columns, values);
        await dbInstance.saveDBItem(dbItem);
    }
}