import { CommandParser } from './CommandParser';
import { RowFilter } from './RowFilter';
import { ColumnFilter } from './ColumnFilter';
import { DB } from './DB';
import { Constants } from './Constants';

export class SelectParser extends CommandParser {

    getSelectField() {
        let lastIndex: number = this._query.indexOf(Constants.whereKeyword);
        if (lastIndex === -1) {
            lastIndex = this._query.length;
        }
        
        let selectFields : string = this._query.substring(this._query.indexOf(Constants.selectKeyword) + Constants.selectKeyword.length, lastIndex).trim();
        if (selectFields[selectFields.length - 1] === ';') {
            selectFields = selectFields.slice(0, -1);
        }

        return selectFields;
    }

    getRowFilter(): RowFilter | undefined {
        if (this._query.indexOf(Constants.whereKeyword) === -1) {
            return;
        }

        let lastIndex: number = this._query.lastIndexOf(';');
        if (lastIndex === -1) {
            lastIndex = this._query.length;
        }
        
        let whereField : string = this._query.substring(this._query.indexOf(Constants.whereKeyword) + Constants.whereKeyword.length, lastIndex).trim();
        let opIndex = whereField.search('[!=<>]');

        return new RowFilter(whereField.substr(0, opIndex).trim(), whereField.substr(opIndex, whereField.substr(opIndex).search('[\'\"a-zA-Z0-9]')).trim(), 
                            whereField.substr(opIndex).substr(whereField.substr(opIndex).search('[\'\"a-zA-Z0-9]')).trim());
    }

    getColumnFilter(): ColumnFilter | undefined {
        let selectFields : string = this.getSelectField();
        if (selectFields === '*') {
            return;
        }

        return new ColumnFilter(selectFields.split(', '));
    }

    async parseQuery() {
        let dbInstance = new DB();
        let filteredData = await dbInstance.getData(this.getRowFilter(), this.getColumnFilter());
        console.log(filteredData);
    }
}