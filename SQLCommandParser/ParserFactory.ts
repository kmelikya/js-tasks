import { InsertParser } from './InsertParser';
import { SelectParser } from './SelectParser';
import { Constants } from './Constants';

export class ParserFactory {

    static getParser(query : string) {
        let mainCommand : string = query.split(' ')[0];
        switch (mainCommand) {
            case Constants.selectKeyword:
                return new SelectParser(query);
            case Constants.insertKeyword:
                return new InsertParser(query);
            default:
                throw new Error("Unsupported query type");
        }
    }

}