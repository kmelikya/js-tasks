function addLoginFunctionality() {
    let loginButton = document.getElementById('login-button');
    if (loginButton) {
        loginButton.addEventListener("click", async function(event) {
            const response = await fetch('http://localhost:8080/login/auth', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                },
                body: JSON.stringify({
                    login: document.getElementById('login-login').value,
                    password: document.getElementById('login-password').value,
                }),
                credentials: 'include'
            });
            if (response.status === 200) {
                alert('Successfully logged in');
            } else {
                alert('Wrong login or password');
            }
        });
    }
}

function addRegisterFunctionality() {
    let registerButton = document.getElementById('register-button');
    if (registerButton) {
        registerButton.addEventListener("click", async function(event) {
            const response = await fetch('http://localhost:8080/register/user', {
                method: 'POST',
                headers: {
                    'Content-type': 'application/json',
                },
                body: JSON.stringify({
                    login: document.getElementById('register-login').value,
                    password: document.getElementById('register-password').value,
                }),
                credentials: 'include'
            });
            if (response.status === 200) {
                alert('Successfully registered');
            } else {
                alert('User with given login already exists');
            }
        });
    }
}

addLoginFunctionality();
addRegisterFunctionality();