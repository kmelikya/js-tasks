import * as http from 'http';
import * as fs from 'fs/promises'
import {Client} from 'pg'
import md5 from 'crypto-js/md5'

enum AuthType {
    Login,
    Register
}

async function getConnectedClient() {
    const client = new Client({
        user: 'postgres',
        host: 'localhost',
        database: 'httpserver',
        password: 'test',
        port: 5432,
    });

    client.connect();

    return client;
}

async function isUserExist(login: string, password: string) {
    
    let retVal: boolean = false;
    const client = await getConnectedClient();

    await new Promise<void>((resolve) => {
        client.query(`SELECT * FROM users WHERE login = $1 AND password = $2`, [login, md5(password).toString()], (err, res) => {

            if (!err && res.rowCount != 0) {
                retVal = true;
            }

            client.end();

            resolve();
        });
    });

    return retVal;
}

async function registerUser(login: string, password: string) {
    
    let retVal: boolean = false;
    const client = await getConnectedClient();

    await new Promise<void>((resolve) => {
        client.query(`INSERT INTO users(login, password) VALUES ($1, $2)`, [login, md5(password).toString()], (err, res) => {

            if (!err) {
                retVal = true;
            }

            client.end();

            resolve();
        })
    });

    return retVal;
}

async function setTokenForUser(login: string, token: string, date: string) {
    const client = await getConnectedClient();

    await new Promise<void>((resolve) => {
        client.query(`UPDATE users SET token = $1, expires = $2 WHERE login = $3`, [token, date, login], (err, res) => {

            if (err) {
                console.log(err);
            }

            client.end();

            resolve();
        })
    });
}

async function validateUser(req: http.IncomingMessage) {

    if (!req.headers['cookie'])
        return false;

    let cookie: string = req.headers['cookie'];
    // we have only token here
    let token: string = cookie.substring(cookie.indexOf('=') + 1);
    const client = await getConnectedClient();
    let retVal: boolean = false;

    await new Promise<void>((resolve) => {
        client.query(`SELECT * FROM users WHERE token = $1`, [token], (err, res) => {

            if (!err && res.rowCount != 0) {
                retVal = true;
            }

            if (new Date(res.rows[0].expires) < new Date()) {
                retVal = false;
            }

            client.end();

            resolve();
        })
    });

    return retVal;
}

async function handlePages(req: http.IncomingMessage, res: http.ServerResponse, pageDir: string = './', pageExtension: string = '') {
    if (req.url === undefined)
        return;
    let page: string = req.url.substr(1);
    console.log(pageDir + page + pageExtension);
    try {
        let body = await fs.readFile(pageDir + page + pageExtension, 'utf-8');
        res.writeHead(200, {
            'Content-Length': body.length
        });
        res.write(body);
    } catch (error) {
        res.writeHead(404, {
            'Content-Length': error.message.length
        });
        res.write(error.message);
    }
    res.end();
}

async function handleCommonPages(req: http.IncomingMessage, res: http.ServerResponse) {
    await handlePages(req, res);
}

async function handleHtmlPages(req: http.IncomingMessage, res: http.ServerResponse) {
    await handlePages(req, res, './pages/', '.html');
}

async function generateToken(length: number) {
    let a: Array<string> = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".split("");
    let b: Array<string> = [];  
    for (let i = 0; i < length; ++i) {
        let j: number = Number((Math.random() * (a.length - 1)).toFixed(0));
        b[i] = a[j];
    }
    return b.join("");
}

async function generateCookie() {
    let tokenLength: number = 128;
    let tokenAgeInDays: number = 7;

    let expires = new Date();
    expires.setDate(expires.getDate() + tokenAgeInDays);

    return {'token': await generateToken(tokenLength), 'Expires': expires.toUTCString()};
}

async function handleAuthRequest(req: http.IncomingMessage, res: http.ServerResponse, authFunction: Function, requestType: AuthType) {
    return new Promise<void>((resolve) => {
        let data = '';
        req.on('data', chunk => {
            data += chunk;
        });

        req.on('end', async () => {
            let body = JSON.parse(data);

            let login = body.login;
            let hashedPassword = body.password;
            try {
                if (login && hashedPassword && (await authFunction(login, hashedPassword))) {
                    if (requestType === AuthType.Login) {
                        console.log('Setting cookies...');
                        let {token, Expires} = await generateCookie();
                        setTokenForUser(login, token, Expires);
                        res.writeHead(200, {
                            'Set-Cookie': `token=${token}; Path=/; Expires=${Expires}`
                        });
                    }
                } else {
                    res.writeHead(403);
                }
            } catch (error) {
                res.writeHead(404, {
                    'Content-Length': error.message.length
                });
                res.write(error);
            }

            res.end();
            resolve();
        });
    })
}

async function handleLoginRequest(req: http.IncomingMessage, res: http.ServerResponse) {
    await handleAuthRequest(req, res, isUserExist, AuthType.Login);
}

async function handleRegisterRequest(req: http.IncomingMessage, res: http.ServerResponse) {
    await handleAuthRequest(req, res, registerUser, AuthType.Register);
}

http.createServer(async function (req, res) {

    if (req.url === undefined)
        return;

    let page: string = req.url.substr(1);

    if (page.startsWith('common')) {
        await handleCommonPages(req, res);
    } else if (page === 'login/auth') {
        await handleLoginRequest(req, res);
    } else if (page === 'register/user') {
        await handleRegisterRequest(req, res);
    } else if (page === 'favicon.ico') {
        // ignore
    } else {
        if (page === 'profile' || page === 'dashboard') {
            if (!await validateUser(req)) {
                // return to login page
                res.end();
                return;
            }
        }
        await handleHtmlPages(req, res);
    }
}).listen(8080);